package com.test.sekolah.usermanagementservice.controller;

import com.test.sekolah.usermanagementservice.config.UserDetailsImpl;
import com.test.sekolah.usermanagementservice.model.dto.request.ReqLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.usermanagementservice.service.AuthService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rest/v1/auth")
public class LoginController {

    private final AuthService authService;

    public LoginController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity<ResMessageDto<ResLoginDto>> login(@Valid @RequestBody ReqLoginDto loginRequest)
    {
        ResMessageDto<ResLoginDto> response = authService.login(loginRequest);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/logout")
    public ResponseEntity<ResMessageDto<String>> logout()
    {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ResMessageDto<String> response = authService.logout(userDetails.getEmail());
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/validate/token")
    ResponseEntity<ResValidateTokenDto> validateAuthToken(@RequestHeader("authToken") String authToken) {
        ResValidateTokenDto response = authService.validate(authToken);
        return ResponseEntity.ok().body(response);
    }
}
