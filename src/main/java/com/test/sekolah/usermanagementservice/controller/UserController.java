package com.test.sekolah.usermanagementservice.controller;

import com.test.sekolah.usermanagementservice.config.UserDetailsImpl;
import com.test.sekolah.usermanagementservice.exception.AuthorizationException;
import com.test.sekolah.usermanagementservice.model.constant.ExceptionMessage;
import com.test.sekolah.usermanagementservice.model.dto.request.ReqInsertUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResGetListUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;
import com.test.sekolah.usermanagementservice.service.UserService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<ResMessageDto<String>> createUser(@Valid @RequestBody ReqInsertUserDto req, Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (Boolean.FALSE.equals(userDetails.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = userService.insertUser(req);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping
    public ResponseEntity<ResMessageDto<String>> updateUser(@RequestParam UUID uuidUser,
                                                            @Valid @RequestBody ReqInsertUserDto req,
                                                            Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (Boolean.FALSE.equals(userDetails.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = userService.updateUser(uuidUser,req);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/list")
    public ResponseEntity<ResMessageDto<List<ResGetListUserDto>>> getListUser(@RequestParam String uuidRole,
                                                                              @RequestParam(required = false) Integer page,
                                                                              @RequestParam(required = false) Integer size,
                                                                              @RequestParam(required = false) String search,
                                                                              Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (Boolean.FALSE.equals(userDetails.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<List<ResGetListUserDto>> response = userService.getListUser(uuidRole, page, size, search);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/detail")
    public ResponseEntity<ResMessageDto<ResGetListUserDto>> getDetailUser(@RequestParam UUID uuidUser,
                                                                          Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (Boolean.FALSE.equals(userDetails.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<ResGetListUserDto> response = userService.getDetailUser(uuidUser);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping
    public ResponseEntity<ResMessageDto<String>> deleteUser(@RequestParam UUID uuidUser,
                                                            Authentication authentication) {
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        if (Boolean.FALSE.equals(userDetails.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = userService.deleteUser(uuidUser);
        return ResponseEntity.ok().body(response);
    }
}
