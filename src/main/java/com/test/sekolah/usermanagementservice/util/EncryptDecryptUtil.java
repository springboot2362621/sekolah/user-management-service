package com.test.sekolah.usermanagementservice.util;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

@Component
public class EncryptDecryptUtil {
    public static String passwordEncrypt(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(11));
    }
}
