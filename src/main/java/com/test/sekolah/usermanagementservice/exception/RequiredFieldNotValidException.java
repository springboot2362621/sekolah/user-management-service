package com.test.sekolah.usermanagementservice.exception;

public class RequiredFieldNotValidException extends RuntimeException {

    private static final long serialVersionUID = 3834385102327726374L;

    public RequiredFieldNotValidException(String message) {
        super(message);
    }
}
