package com.test.sekolah.usermanagementservice.exception;

public class RequiredFieldIsMissingException extends RuntimeException {

    private static final long serialVersionUID = 4123371684538937348L;

    public RequiredFieldIsMissingException(String message) {
        super(message);
    }

}
