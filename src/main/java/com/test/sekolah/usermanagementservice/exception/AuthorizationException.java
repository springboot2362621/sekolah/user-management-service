package com.test.sekolah.usermanagementservice.exception;

public class AuthorizationException extends RuntimeException{

    private static final long serialVersionUID = 4867193963080361998L;
    public AuthorizationException(String message) {
        super(message);
    }
}
