package com.test.sekolah.usermanagementservice.repository;

import com.test.sekolah.usermanagementservice.model.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<RoleEntity, UUID> {
    Set<RoleEntity> findByUuidIn(List<UUID> uuid);
}
