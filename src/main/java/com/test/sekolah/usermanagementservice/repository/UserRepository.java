package com.test.sekolah.usermanagementservice.repository;

import com.test.sekolah.usermanagementservice.model.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {
    Optional<UserEntity> findByEmail(String email);

    Optional<UserEntity>findByEmailAndActiveIsTrue(String email);
    @Query("SELECT UE " +
            "FROM UserEntity UE " +
            "WHERE (:search is null OR :search = '' OR " +
            "LOWER(UE.name || UE.email || UE.phoneNumber) LIKE LOWER(CONCAT('%',:search,'%'))) " +
            "AND UE.active = true " +
            "AND UE.roles.uuid = :uuidRole "+
            "ORDER BY UE.lastModifiedDate DESC")
    Page<UserEntity> getListUser(@Param("search") String search,
                                 @Param("uuidRole") UUID uuidRole,
                                 Pageable pageable);
}
