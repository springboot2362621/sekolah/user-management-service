package com.test.sekolah.usermanagementservice.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.test.sekolah.usermanagementservice.model.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class UserDetailsImpl implements UserDetails {
    private UUID uuid;
    private String name;
    private String email;
    private String phoneNumber;
    private String roleName;
    private Boolean isLogin;
    @JsonIgnore
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailsImpl(UUID uuid, String name, String email, String phoneNumber, String roleName,
                           Boolean isLogin,String password, Collection<? extends GrantedAuthority> authorities) {
        this.uuid = uuid;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isLogin = isLogin;
        this.roleName = roleName;
        this.password = password;
        this.authorities = authorities;
    }

    public static UserDetailsImpl build(UserEntity user) {
        List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(user.getRoles().getName()));
        return new UserDetailsImpl(
                user.getUuid(),
                user.getName(),
                user.getEmail(),
                user.getPhoneNumber(),
                user.getRoles().getName(),
                user.isLogin(),
                user.getPassword(),
                authorities
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
