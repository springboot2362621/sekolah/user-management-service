package com.test.sekolah.usermanagementservice.model.dto.response;

import lombok.Data;

@Data
public class ResLoginDto {
    private String idUser;
    private String token;
    private String name;
    private String email;
    private String phoneNumber;
    private String roleName;
}
