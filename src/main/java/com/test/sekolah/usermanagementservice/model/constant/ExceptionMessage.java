package com.test.sekolah.usermanagementservice.model.constant;

public class ExceptionMessage {
    public static final String EMAIL_DUPLICATE = "Found other user with same email";
    public static final String UUID_ROLE_NOT_FOUND = "UUID role Not Found with uuid ";
    public static final String UUID_USER_NOT_FOUND = "UUID user Not Found with uuid ";
    public static final String EMAIL_NOT_FOUND = "Email Not Found with uuid ";
    public static final String BEARER_TOKEN_INVALID = "Token is not valid.";
    public static final String INVALID_PERMISSION_API = "You dont have permission to access this API";
    public static final String TOKEN_FOR_VALIDATE_IS_MISSING = "Token for login is missing.";
}
