package com.test.sekolah.usermanagementservice.model.dto.response;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class ResGetListUserDto {
    private String uuidUser;
    private String name;
    private String email;
    private String phoneNumber;
    private String role;
}
