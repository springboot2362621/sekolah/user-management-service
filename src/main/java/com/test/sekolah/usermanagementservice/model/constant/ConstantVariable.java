package com.test.sekolah.usermanagementservice.model.constant;

public class ConstantVariable {
    public static final String SUCCESS = "Success";
    public static final Integer STATUS_SUCCESS = 200;
    public static final String BEARER = "Bearer ";
}
