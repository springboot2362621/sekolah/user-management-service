package com.test.sekolah.usermanagementservice.model.dto.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class ReqInsertUserDto {
    @NotEmpty(message = "User name is missing.")
    private String name;

    @NotEmpty(message = "User email is missing.")
    private String email;

    @NotEmpty(message = "User phone is missing.")
    private String phoneNumber;

    @NotEmpty(message = "User password is missing.")
    private String password;

    @NotEmpty(message = "UUID Role is missing.")
    private String uuidRole;
}
