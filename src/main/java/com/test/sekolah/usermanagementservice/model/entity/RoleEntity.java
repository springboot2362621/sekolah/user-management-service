package com.test.sekolah.usermanagementservice.model.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="mst_roles")
public class RoleEntity extends BaseEntity {

    @Column(name = "name", length = 60, nullable = false, unique = false)
    private String name;
}
