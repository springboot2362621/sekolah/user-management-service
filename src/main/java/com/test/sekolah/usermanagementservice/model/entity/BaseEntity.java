package com.test.sekolah.usermanagementservice.model.entity;


import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public class BaseEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(length = 36, updatable = false, nullable = false )
    private UUID uuid;

    @CreationTimestamp
    @Column(name="created_at", nullable=false, updatable = false)
    private Timestamp createdDate;

    @UpdateTimestamp
    @Column(name="updated_at", nullable=true)
    private Timestamp lastModifiedDate;

    @Column(name="is_active",columnDefinition = "boolean default true")
    private boolean active=true;
}
