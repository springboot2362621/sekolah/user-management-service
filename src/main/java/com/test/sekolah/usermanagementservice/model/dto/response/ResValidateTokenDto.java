package com.test.sekolah.usermanagementservice.model.dto.response;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class ResValidateTokenDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 7122800465244952356L;
    private String idUser;
    private String name;
    private String email;
    private String phoneNumber;
    private String roleName;
    private Boolean isLogin;
}
