package com.test.sekolah.usermanagementservice.model.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="mst_users")
public class UserEntity extends BaseEntity{

    @Column(name="name", length=60, nullable=false, unique=false)
    private String name;

    @Column(name="email", length=100, nullable=false, unique=true)
    private String email;

    @Column(name="password", length=200, nullable=true, unique=false)
    private String password;

    @Column(name="phone_number", length=20, nullable=true, unique=false)
    private String phoneNumber;

    @Column(name="is_login", columnDefinition = "boolean default false")
    private boolean login;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private RoleEntity roles;
}
