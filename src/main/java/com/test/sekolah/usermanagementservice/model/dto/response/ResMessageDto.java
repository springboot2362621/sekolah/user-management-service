package com.test.sekolah.usermanagementservice.model.dto.response;

import com.test.sekolah.usermanagementservice.model.constant.ConstantVariable;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonInclude;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResMessageDto<T> {
    private int status=ConstantVariable.STATUS_SUCCESS;
    private String message= ConstantVariable.SUCCESS;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;


}
