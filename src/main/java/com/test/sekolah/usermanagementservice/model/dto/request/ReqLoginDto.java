package com.test.sekolah.usermanagementservice.model.dto.request;

import lombok.Data;

@Data
public class ReqLoginDto {
    private String username;
    private String password;
}
