package com.test.sekolah.usermanagementservice.service;

import com.test.sekolah.usermanagementservice.model.dto.request.ReqInsertUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResGetListUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;

import java.util.List;
import java.util.UUID;

public interface UserService {

    ResMessageDto<String> insertUser(ReqInsertUserDto request);
    ResMessageDto<String> updateUser(UUID uuidUser,ReqInsertUserDto request);
    ResMessageDto<List<ResGetListUserDto>> getListUser(String uuidRole, Integer page, Integer size, String search);
    ResMessageDto<ResGetListUserDto> getDetailUser(UUID uuidUser);
    ResMessageDto<String> deleteUser(UUID uuidUser);
}
