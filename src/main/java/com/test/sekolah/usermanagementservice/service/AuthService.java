package com.test.sekolah.usermanagementservice.service;

import com.test.sekolah.usermanagementservice.model.dto.request.ReqLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResValidateTokenDto;

import java.util.UUID;

public interface AuthService {
    ResMessageDto<ResLoginDto> login(ReqLoginDto reqLoginDto);
    ResMessageDto<String> logout(String email);
    void setLoginTrue(UUID uuid);
    ResValidateTokenDto validate(String authToken);
}
