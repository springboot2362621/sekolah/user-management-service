package com.test.sekolah.usermanagementservice.service.impl;

import com.test.sekolah.usermanagementservice.exception.DataNotFoundException;
import com.test.sekolah.usermanagementservice.exception.DuplicateException;
import com.test.sekolah.usermanagementservice.exception.RequiredFieldIsMissingException;
import com.test.sekolah.usermanagementservice.model.constant.ConstantVariable;
import com.test.sekolah.usermanagementservice.model.constant.ExceptionMessage;
import com.test.sekolah.usermanagementservice.model.dto.request.ReqInsertUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResGetListUserDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;
import com.test.sekolah.usermanagementservice.model.entity.RoleEntity;
import com.test.sekolah.usermanagementservice.model.entity.UserEntity;
import com.test.sekolah.usermanagementservice.repository.RoleRepository;
import com.test.sekolah.usermanagementservice.repository.UserRepository;
import com.test.sekolah.usermanagementservice.service.UserService;
import com.test.sekolah.usermanagementservice.util.EncryptDecryptUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public ResMessageDto<String> insertUser(ReqInsertUserDto request) {
        Optional<UserEntity> emailExiting = userRepository.findByEmail(request.getEmail());
        if (emailExiting.isPresent()){
            throw new DuplicateException(ExceptionMessage.EMAIL_DUPLICATE);
        }

        Optional<RoleEntity> roleOpt = roleRepository.findById(UUID.fromString(request.getUuidRole()));
        if (roleOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_ROLE_NOT_FOUND);
        }

        UserEntity data = new UserEntity();
        data.setName(request.getName());
        data.setEmail(request.getEmail());
        data.setPhoneNumber(request.getPhoneNumber());
        data.setPassword(EncryptDecryptUtil.passwordEncrypt(request.getPassword()));
        data.setRoles(roleOpt.get());

        userRepository.save(data);

        return new ResMessageDto<>();

    }

    @Override
    public ResMessageDto<String> updateUser(UUID uuidUser, ReqInsertUserDto request) {
        Optional<UserEntity> userExiting = userRepository.findById(uuidUser);
        if (userExiting.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_USER_NOT_FOUND);
        }

        Optional<RoleEntity> roleOpt = roleRepository.findById(UUID.fromString(request.getUuidRole()));
        if (roleOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_ROLE_NOT_FOUND);
        }

        Optional<UserEntity> emailExiting = userRepository.findByEmail(request.getEmail());
        if (emailExiting.isPresent() && emailExiting.get().getEmail() != userExiting.get().getEmail()){
            throw new DuplicateException(ExceptionMessage.EMAIL_DUPLICATE);
        }

        UserEntity data = userExiting.get();
        data.setEmail(request.getEmail());
        data.setPassword(EncryptDecryptUtil.passwordEncrypt(request.getPassword()));
        data.setName(request.getName());
        data.setPhoneNumber(request.getPhoneNumber());
        data.setRoles(roleOpt.get());
        userRepository.save(data);
        return new ResMessageDto<>();
    }

    @Override
    public ResMessageDto<List<ResGetListUserDto>> getListUser(String uuidRole, Integer page, Integer size, String search) {
        if (uuidRole.isEmpty()){
            throw new RequiredFieldIsMissingException("uuidRole is Missing");
        }

        Optional<RoleEntity> roleOpt = roleRepository.findById(UUID.fromString(uuidRole));
        if (roleOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_ROLE_NOT_FOUND + uuidRole);
        }
        int pageReq = (page != null && size != null) ? page - 1 : 0;
        int sizeReq = (page != null && size != null) ? size : Integer.MAX_VALUE;
        Pageable pageable = PageRequest.of(pageReq, sizeReq);

        List<ResGetListUserDto> result = new ArrayList<>();

        Page<UserEntity> listUser = userRepository.getListUser(search,roleOpt.get().getUuid(),pageable);
        for (UserEntity user : listUser){
            ResGetListUserDto data = new ResGetListUserDto();
            data.setUuidUser(user.getUuid().toString());
            data.setName(user.getName());
            data.setEmail(user.getEmail());
            data.setPhoneNumber(user.getPhoneNumber());
            data.setRole(user.getRoles().getName());

            result.add(data);
        }
        return ResMessageDto.<List<ResGetListUserDto>>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<ResGetListUserDto> getDetailUser(UUID uuidUser) {
        Optional<UserEntity> userOpt = userRepository.findById(uuidUser);
        if (userOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_USER_NOT_FOUND + uuidUser);
        }
        UserEntity userExiting = userOpt.get();
        ResGetListUserDto result = new ResGetListUserDto();
        result.setUuidUser(String.valueOf(userExiting.getUuid()));
        result.setName(userExiting.getName());
        result.setEmail(userExiting.getEmail());
        result.setPhoneNumber(userExiting.getPhoneNumber());
        result.setRole(userExiting.getRoles().getName());

        return ResMessageDto.<ResGetListUserDto>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<String> deleteUser(UUID uuidUser) {
        Optional<UserEntity> userOpt = userRepository.findById(uuidUser);
        if (userOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_USER_NOT_FOUND + uuidUser);
        }

        UserEntity user = userOpt.get();
        user.setActive(false);
        userRepository.save(user);
        return new  ResMessageDto<>();
    }
}
