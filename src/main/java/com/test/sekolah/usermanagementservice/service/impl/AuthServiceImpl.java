package com.test.sekolah.usermanagementservice.service.impl;

import com.test.sekolah.usermanagementservice.config.UserDetailsImpl;
import com.test.sekolah.usermanagementservice.exception.AuthorizationException;
import com.test.sekolah.usermanagementservice.exception.DataNotFoundException;
import com.test.sekolah.usermanagementservice.exception.RequiredFieldIsMissingException;
import com.test.sekolah.usermanagementservice.exception.RequiredFieldNotValidException;
import com.test.sekolah.usermanagementservice.model.constant.ConstantVariable;
import com.test.sekolah.usermanagementservice.model.constant.ExceptionMessage;
import com.test.sekolah.usermanagementservice.model.dto.request.ReqLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResLoginDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResMessageDto;
import com.test.sekolah.usermanagementservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.usermanagementservice.model.entity.UserEntity;
import com.test.sekolah.usermanagementservice.repository.UserRepository;
import com.test.sekolah.usermanagementservice.security.JwtUtils;
import com.test.sekolah.usermanagementservice.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;


    private final JwtUtils jwtUtils;
    public AuthServiceImpl(UserRepository userRepository, AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
    }

    @Override
    public ResMessageDto<ResLoginDto> login(ReqLoginDto reqLoginDto) {
        try {
            Authentication authentication = null;
            UserDetailsImpl userDetails = null;
            if (reqLoginDto.getPassword() != null && !reqLoginDto.getPassword().isBlank()) {
                authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(reqLoginDto.getUsername(),
                                reqLoginDto.getPassword()));
                userDetails = (UserDetailsImpl) authentication.getPrincipal();
            }

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            ResLoginDto data = new ResLoginDto();
            data.setIdUser(String.valueOf(userDetails.getUuid()));
            data.setToken(jwt);
            data.setName(userDetails.getName());
            data.setEmail(userDetails.getEmail());
            data.setPhoneNumber(userDetails.getPhoneNumber());
            data.setRoleName(userDetails.getRoleName());

            setLoginTrue(userDetails.getUuid());
            return new  ResMessageDto(200, "Success", data);
        } catch (Exception e) {
            String respon = "Bad Credentials";
            throw new AuthorizationException(respon);
        }
    }

    @Override
    public ResMessageDto<String> logout(String email) {
        Optional<UserEntity> userOpt = userRepository.findByEmailAndActiveIsTrue(email);
        if (userOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.EMAIL_NOT_FOUND);
        }

        UserEntity user = userOpt.get();
        user.setLogin(false);
        userRepository.save(user);
        SecurityContextHolder.clearContext();
        return new ResMessageDto<>();
    }

    @Override
    public void setLoginTrue(UUID uuid) {
        Optional<UserEntity> userOpt = userRepository.findById(uuid);
        if (userOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.UUID_USER_NOT_FOUND + uuid);
        }

        UserEntity data = userOpt.get();
        data.setLogin(true);
        userRepository.save(data);
    }

    @Override
    public ResValidateTokenDto validate(String authToken) {
        if (!StringUtils.hasText(authToken))
            throw new RequiredFieldIsMissingException(ExceptionMessage.TOKEN_FOR_VALIDATE_IS_MISSING);


        if (authToken.length() < 8)
            throw new RequiredFieldNotValidException(ExceptionMessage.BEARER_TOKEN_INVALID);


        String token = "";
        if (authToken.startsWith(ConstantVariable.BEARER))
            token = authToken.substring(7, authToken.length());


        ResValidateTokenDto resValidatetoken = new ResValidateTokenDto();
        if (!token.isBlank() && jwtUtils.validateJwtToken(token)) {
            String email = jwtUtils.getUserNameFromJwtToken(token);

            Optional<UserEntity> userDetails = userRepository.findByEmailAndActiveIsTrue(email);
            if (userDetails.isPresent()) {
                resValidatetoken.setIdUser(String.valueOf(userDetails.get().getUuid()));
                resValidatetoken.setName(userDetails.get().getName());
                resValidatetoken.setEmail(userDetails.get().getEmail());
                resValidatetoken.setPhoneNumber(userDetails.get().getPhoneNumber());
                resValidatetoken.setRoleName(userDetails.get().getRoles().getName());
                resValidatetoken.setIsLogin(userDetails.get().isLogin());
            }
        }
        return resValidatetoken;
    }
}
